#!/bin/ash

[[ ! -f EXISTING ]] || touch EXISTING
EXISTING=$(cat EXISTING)
echo "Existing: ${EXISTING}"
LATEST=$(git rev-parse origin/master | cut -c -7)
echo "Latest: ${LATEST}"
if [[ "${LATEST}" == "${EXISTING}" ]]; then
  echo "Skipping..."
  exit 0
fi

apk add curl jq

BUILDX_VER=$(curl -ks https://api.github.com/repos/docker/buildx/releases/latest | jq -r '.name')
mkdir -p "$HOME/.docker/cli-plugins/"
wget -O "$HOME/.docker/cli-plugins/docker-buildx" "https://github.com/docker/buildx/releases/download/${BUILDX_VER}/buildx-${BUILDX_VER}.linux-amd64"
chmod a+x "$HOME/.docker/cli-plugins/docker-buildx"
echo -e '{\n  "experimental": "enabled"\n}' | tee "$HOME/.docker/config.json"
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

docker buildx create --use --name builder
docker buildx inspect --bootstrap builder
docker buildx install

docker buildx build --cache-to=type=local,dest=cache,mode=max --platform linux/amd64,linux/arm64,linux/arm/v7 -t "${REGISTRY_IMAGE}:latest" -t "${REGISTRY_IMAGE}:${LATEST}" .
echo "${TOKEN}" | docker login -u "${USERNAME}" --password-stdin ${REGISTRY}
docker buildx build --push --cache-from=type=local,src=cache --platform linux/amd64,linux/arm64,linux/arm/v7 -t "${REGISTRY_IMAGE}:latest" -t "${REGISTRY_IMAGE}:${LATEST}" .
docker buildx build --push --cache-from=type=local,src=cache --platform linux/amd64,linux/arm64,linux/arm/v7 -t "${REGISTRY_IMAGE2}:latest" -t "${REGISTRY_IMAGE2}:${LATEST}" .

echo "${LATEST}" > EXISTING
