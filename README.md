![](https://images.microbadger.com/badges/version/jarylc/snapdrop.svg) ![](https://images.microbadger.com/badges/image/jarylc/snapdrop.svg) ![](https://img.shields.io/docker/stars/jarylc/snapdrop.svg) ![](https://img.shields.io/docker/pulls/jarylc/snapdrop.svg)

Try setting `SINGLE_ROOM` to 1 if you cannot see other peers, note that this would make everyone able to see each other regardless of their connection method.

# Environment variables:
| Environment | Default value | Description
|---|---|---|
| HTTP_PORT | 8080 | Port of internal Lighttpd proxy, set to `0` to disable
| PORT | 3000 | Port of Snapdrop WebSocket server
| SINGLE_ROOM | 0 | Single room mode (all peers see each other on the same server regardless of network)
| STUN_SERVER | stun:stun.l.google.com:19302 | STUN server URL

# Deploying
## 1. Docker
### Terminal
```bash
docker run -d \
    --name snapdrop \
    -e HTTP_PORT=8080 \
    -e PORT=3000 \
    -e SINGLE_ROOM=0 \
    -e STUN_SERVER=stun:stun.l.google.com:19302 \
    -p 3000:3000 `# optional: only if not using Lighttpd proxy in container` \
    -p 8080:8080 \
    -v /var/www/snapdrop:/home/node/client `# optional: only if not using Lighttpd proxy in container` \
    --restart unless-stopped \
    minimages/snapdrop
```
### Docker-compose
```yml
snapdrop:
    image: minimages/snapdrop
    ports:
        - "3000:3000" # optional: only if not using Lighttpd proxy in container
        - "8080:8080"
    volumes: # optional: only if not using Lighttpd proxy in container
        - /var/www/snapdrop:/home/node/client
    environment:
        - HTTP_PORT=8080
        - PORT=3000
        - SINGLE_ROOM=0
        - STUN_SERVER=stun:stun.l.google.com:19302
    restart: unless-stopped
```

## 2a. Deployment through container Lighttpd only
Just assign the container's Lighttpd port (`HTTP_PORT`) to the designated port on the host you will access Snapdrop with.

## 2b. Deployment through container Lighttpd and host Nginx
```nginx
location / { # trailing slash is required if under a subpath
    proxy_pass http://127.0.0.1:8080/; # trailing slash is important if under a subpath
    proxy_connect_timeout 300;
    proxy_http_version 1.1;
    proxy_set_header Connection "upgrade";
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header X-Forwarded-for $remote_addr;
}
```

## 2c. Deployment only on host Nginx
### Under /
```nginx
location / {
    root /var/www/snapdrop;
    index index.html;
}

location /server {
    proxy_pass http://127.0.0.1:3000;
    proxy_connect_timeout 300;
    proxy_http_version 1.1;
    proxy_set_header Connection "upgrade";
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header X-Forwarded-for $remote_addr;
}
```
### Under /snapdrop
```nginx
location /snapdrop {
    alias /var/www/snapdrop;
    index index.html;
}

location /snapdrop/server {
    proxy_pass http://127.0.0.1:3000;
    proxy_connect_timeout 300;
    proxy_set_header Connection "upgrade";
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header X-Forwarded-for $remote_addr;
}
```
